package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForCreateFwUpgradeTask extends BaseResponse {
    private String operationId;// 操作任务ID
}
