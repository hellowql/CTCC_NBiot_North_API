package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryDeviceHistoryData {
    @Required
    private String deviceId;//设备唯一标识，1-64个字节
    @Required
    private String gatewayId;//查询参数，网关的设备唯一标识。当设备是直连设备时，gatewayId为设备本身的deviceId。当设备是非直连设备时，gatewayId为设备所关联的直连设备（即网关）的deviceId。
    private String serviceId;//服务Id
    private String property;//服务属性数据
    private String appId;//应用唯一标识
    private Integer pageNo;//分页查询参数， pageNo=null时查询内容不分页；取值大于等于0的整数时分页查询，等于0时查询第一页
    private Integer pageSize;//分页查询参数，取值大于等于1的整数，缺省值为1，设备历史数据最多保存10万条，这个数值有可能被运营商修改
    private String startTime;//查询参数，根据时间段查询的起始时间; 时间格式：yyyyMMdd’T’HHmmss’Z’ 如： 20151212T121212Z，设备历史数据最多保存90天，这个数值有可能被运营商修改
    private String endTime;//查询参数，根据时间段查询的结束时间; 时间格式：yyyyMMdd’T’HHmmss’Z’ 如： 20151212T121212Z
}
