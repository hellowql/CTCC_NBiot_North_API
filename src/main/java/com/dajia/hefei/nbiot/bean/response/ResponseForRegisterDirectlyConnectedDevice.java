package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForRegisterDirectlyConnectedDevice extends BaseResponse {
    private String deviceId;
    private String verifyCode;
    private String psk;
    private Integer timeout;
}
