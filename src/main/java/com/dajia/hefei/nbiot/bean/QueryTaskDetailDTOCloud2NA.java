package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class QueryTaskDetailDTOCloud2NA {
    private String status;// 任务详情状态，取值范围：Pending/WaitResult/Success/Fail/Timeout
    private String output;// 批量注册设备、批量下发命令或批量修改设备位置的输出信息。
    private String error;// 任务的错误原因，格式为：{"error_code":"****","error_desc":"****"}
    private Object param;// 不同任务类型的具体参数不同

    @Data
    public static class DeviceReg {
        private String index;// 批量任务文件里第几行的任务。
        private String nodeId;// 设备的唯一标识。
    }

    @Data
    public static class Command {
        private String deviceId;// 设备Id。
        private String commandId;// 下发的命令Id。
    }
}
