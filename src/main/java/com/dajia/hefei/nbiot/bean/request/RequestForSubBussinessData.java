package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.NOTIFYTYPE;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForSubBussinessData {
    @Required
    private NOTIFYTYPE notifyType;// 通知类型，第三方应用可以根据通知类型对消息分别进行处理
    @Required
    private String callbackUrl;// 订阅的回调地址，用于接收对应类型的通知消息。必须使用HTTPS信道回调地址，同时回调地址中必须指定回调地址的端口。
    private String ownerFlag;// callbackUrl的所有者标识。false：表示callbackUrl的owner是授权应用；true：表示callbackUrl的owner为被授权用户
}
