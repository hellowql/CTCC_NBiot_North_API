package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForModifyDeviceGroup {
    private String accessAppId;// 当修改授权应用下的设备组时需要填写，填写授权应用的应用ID
    @Required
    private String devGroupId;// 设备组ID，在添加设备组后由IoT平台返回获得。
    @Required
    private String name;// 设备组名称，仅限大小写字母和数字。
    private String description;// 设备组的描述信息。
    private Integer maxDevNum;// 设备组设备最大数量，默认值为0.当值为0时，表示对设备数量不做限制。
}
