package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.COMMAND_STATUS;
import com.dajia.hefei.nbiot.bean.CommandDTOV4;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForModifyDeviceCommand extends BaseResponse {
    private String commandId;// 设备命令ID
    private String appId;
    private String deviceId;// 下发命令的设备ID，用于唯一标识一个设备
    private CommandDTOV4 command;// 下发命令的信息
    private String callbackUrl;// 命令状态变化通知地址
    private Integer expireTime;// 下发命令的有效时间，单位为秒，表示设备命令在创建expireTime秒内有效，超过这个时间范围后命令将不再下发，如果未设置则默认未48小时。如果expireTime设置为0，则无论IoT平台上设置的命令下发模式是什么，该命令都会立即下发给设备（如果设备处于休眠状态或者链路已老化，则设备收不到命令，平台没收到设备的响应，该命令最终会超时）
    private COMMAND_STATUS status;// 下发命令的状态
    private Object result;// 下发命令执行的详细结果
    private String creationTime;// 命令的创建时间
    private String executeTime;// 命令执行的时间
    private String platformIssuedTime;// 平台发送命令的时间
    private String deliveredTime;// 平台将命令送达到设备的时间
    private Integer issuedTimes;// 平台发送命令的次数。范围大于等于0
    private Integer maxRetransmit;// 命令下发最大重传次数。范围0~3
}
