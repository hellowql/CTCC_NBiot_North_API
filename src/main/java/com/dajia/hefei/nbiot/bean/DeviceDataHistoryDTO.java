package com.dajia.hefei.nbiot.bean;

import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

@Data
public class DeviceDataHistoryDTO {
    private String serviceId;//	服务ID
    private String deviceId;// 设备唯一标识，1-64个字节
    private String gatewayId;// 网关的设备唯一标识，1-64个字节。当设备是直连设备时，gatewayId为设备本身的deviceId。当设备是非直连设备时，gatewayId为设备所关联的直连设备（即网关）的deviceId。
    private String appId;//	应用程序ID
    private Object data;// 设备上报的数据，数据是键值对，取值请参见Profile文件中服务能力表里面的propertyties。
    private String timestamp;//	上报数据的时间戳 时间格式：yyyyMMdd’T’HHmmss’Z’ 如：    20151212T121212Z

    public String getTimestamp() {
        return DateTimeUtil.TZ2Punc(timestamp);
    }
}
