package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Optional;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryDevice {
    @Required
    private String deviceId;// 设备ID，用于唯一标识一个设备，在注册设备时由IoT平台分配获得。
    @Optional
    private String select;// 指定可选的返回值，可取值：imsi。
}
