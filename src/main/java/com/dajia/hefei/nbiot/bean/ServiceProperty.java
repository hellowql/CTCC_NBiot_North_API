package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class ServiceProperty {
    private String propertyName;// 属性名称
    private String dataType;// 数据类型
    private boolean required;//	是否必选
    private String min;// 最小
    private String max;// 最大
    private Double step;// 步长
    private Integer maxLength;// 最大长度
    private String method;// 访问方法：RWE 可读R，可写W，可观察E
    private String unit;// 单位（符号）
    private List<String> enumList;// 枚举类型列表
}
