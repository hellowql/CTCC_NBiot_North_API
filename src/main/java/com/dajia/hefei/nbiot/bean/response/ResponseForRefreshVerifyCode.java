package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForRefreshVerifyCode extends BaseResponse {
    private String verifyCode;// 验证码，设备可以通过验证码获取设备ID和密码。若在请求中指定verifyCode，则响应中返回请求中指定的verifyCode；若请求中不指定verifyCode，则由IoT平台自动生成。
    private Integer timeout;// 验证码有效时间，单位秒，设备需要在有效时间内接入IoT平台。
}
