package com.dajia.hefei.nbiot.bean;

public enum NODE_TYPE {
    ENDPOINT,
    GATEWAY,
    UNKNOW
}
