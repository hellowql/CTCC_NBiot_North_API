package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.NOTIFYTYPE;
import com.dajia.hefei.nbiot.bean.Optional;
import lombok.Data;

@Data
public class RequestForDeleteSubs {
    @Optional
    private NOTIFYTYPE notifytype;// 通知类型
    private String callbackUrl;// 订阅回调的URL地址
}
