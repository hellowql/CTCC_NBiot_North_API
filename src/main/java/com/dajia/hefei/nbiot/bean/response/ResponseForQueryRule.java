package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.bean.RuleDTO;
import com.dajia.hefei.nbiot.bean.TagDTO2;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryRule extends BaseResponse {
    private String ruleId;// 规则实例的ID，仅在规则更新时有效，创建规则时不填写。
    private String appKey;
    @Required
    private String name;// 规则名称
    private String description;// 规则描述
    @Required
    private String author;// 创建此规则的用户的ID
    private List<Object> conditions;// 条件列表，与groupExpress二选一必填，具体参见，ConditionDeviceData,ConditionDeviceGroupData,ConditionDeviceTypeData,ConditionDailyTimer,ConditionCycleTimer,ConditionNoDetected
    private RuleDTO.OPERATOR logic;// 多条件之间的逻辑关系，支持and和or，默认为and
    private RuleDTO.TimeRange timeRange;// 条件场景的时间段
    // 定时场景必选
    private List<Object> actions;// 满足规则所执行的动作，具体参加ActionDeviceCMD,ActionSMS,ActionEmail,ActionDelay,ActionRule,ActionDeviceAlarm,ActionEICMD
    private String matchNow;// 表示是否立即触发，即是否立即进行规则条件判断，条件符合的话执行动作。yes：立即触发；no：不触发
    private String status;// 规则的状态，默认为active状态。active：激活，inactive：未激活
    private RuleDTO.GroupExpress groupExpress;// 复杂多条件表达式，具体参见GroupExpress结构体，与conditions二选一必填。
    private String timezoneID;// 时区ID。若为空，使用UTC时间；若不为空，使用本地时间。
    private List<Object> triggerSources;// 触发源列表，只与GroupExpress联用构造复杂多条件规则，目前有DEVICE类型和TIMER类型，具体参加TriggerSourceDevice和TriggerSourceTimer
    private String executor;// 规则执行主体，当action和condition里面的设备在同一个网关下面时，取值网管ID（代表网关执行）；否则取定值“cloud”（代表云端执行）。
    private Object transData;// 平台不需要识别的数据，只做保存
    private Boolean refreshId;// 穿件规则时是否检查ruleId不能为空标识，默认为true
    private Boolean checkNullAction;// 创建规则时是否检查Action不能为空标识，默认为true
    private String priority;// 规则优先级，保留字段。
    private List<TagDTO2> tags;// 标签列表
    private List<RuleDTO.RulePreProcessorDTO> rulePreProcessors;// 规则的预处理器，不符合预处理条件的规则不执行
}
