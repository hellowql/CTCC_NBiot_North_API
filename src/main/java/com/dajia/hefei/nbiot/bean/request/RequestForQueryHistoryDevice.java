package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryHistoryDevice {
    @Required
    private String deviceId;// 设备ID，用于唯一标识一个设备
    @Required
    private String gatewayId;// 网关ID，用于标识一个网关设备
    private String serviceId;// 设备的服务标识
    private String property;// 服务属性数据
    private Integer pageNo;// 查询的页码
    private Integer pageSize;// 查询每页信息的数量，缺省值：1
    private String startTime;// 查询产生时间在startTime之后的历史数据
    private String endTime;// 查询产生时间在endTime之前的历史数据
}
