package com.dajia.hefei.nbiot.bean;

public enum DEVICE_STATUS {
    ONLINE/*在线*/,
    OFFLINE/*不在线*/,
    INBOX,
    ABNORMAL/*异常状态*/,
}
