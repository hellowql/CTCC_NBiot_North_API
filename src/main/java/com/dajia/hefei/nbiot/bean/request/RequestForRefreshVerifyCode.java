package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForRefreshVerifyCode {
    @Required
    private String deviceId;// 设备ID，用于唯一标识一个设备，在注册设备时由IoT平台分配获得。
    private String verifyCode;// 设备验证码，若在请求中指定verifyCode，则响应中返回请求中指定的verifyCode；若请求中不指定verifyCode，则由IoT平台自动生成。建议和nodeId设置成一样。
    private String nodeId;// 设备的唯一标识，通常使用MAC，MAC地址，Serial No或IMEI作为nodeId。值空时，nodeId不变。值不为空时，更新nodeId。
    private Integer timeout;// 设备验证码有效期，单位秒，默认值180s。值为0时，表示验证码则永不过期。值为大于等于0的整数时，表示在指定时间内设备进行绑定，超过时间验证码无效，同时注册的设备会被删除。
}
