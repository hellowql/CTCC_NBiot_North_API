package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class DataConfigDTO {
    private Integer dataAgingTime;//数据老化时长配置，取值：0~90，单位：天
}
