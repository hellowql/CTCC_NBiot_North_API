package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.OperateDevices;
import com.dajia.hefei.nbiot.bean.OperatePolicy;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForCreateSwUpgradeTask {
    @Required
    private String fileId;// 要升级的目标版本包ID
    @Required
    private OperateDevices targets;// 要进行升级的目标
    private OperatePolicy policy;// 升级任务的执行策略
}
