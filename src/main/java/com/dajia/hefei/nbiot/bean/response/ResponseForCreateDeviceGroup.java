package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForCreateDeviceGroup extends BaseResponse {
    private String name;// 设备组名称
    private String description;// 设备组的描述信息
    private String id;// 设备组ID，由平台自动生成
    private String appId;
    private Integer maxDevNum;// 设备组设备最大数量限制，当值为0时，表示对设备数量不做限制
    private Integer curDevNum;// 当前设备组内的设备数量
}
