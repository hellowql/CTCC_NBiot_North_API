package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfDeviceDeleted {
    @Required
    private String notifyType;// 通知类型，取值：deviceDeleted
    @Required
    private String deviceId;// 设备ID
    @Required
    private String gatewayId;// 网关ID
}
