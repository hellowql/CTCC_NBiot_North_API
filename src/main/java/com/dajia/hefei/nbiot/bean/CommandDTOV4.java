package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class CommandDTOV4 {
    @Required
    private String serviceId;// 命令对应的服务ID，用于标识一个服务。要与profile中定义的serviceId保持一致。
    @Required
    private String method;// 命令服务下具体的命令名称，服务属性等。要与profile中定义的命令名保持一致。
    @Required
    private Object paras;// 命令参数的jsonString，具体格式需要应用和设备约定
}
