package com.dajia.hefei.nbiot.bean;

import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

@Data
public class DeviceServiceData {
    @Required
    private String serviceId;// 服务ID
    @Required
    private String serviceType;// 服务的类型
    @Required
    private Object data;// 服务数据信息
    @Required
    private String eventTime;// 事件发生时间

    public String getEventTime() {
        return DateTimeUtil.TZ2Punc(this.eventTime);
    }
}
