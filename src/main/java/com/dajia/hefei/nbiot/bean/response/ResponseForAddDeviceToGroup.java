package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForAddDeviceToGroup extends BaseResponse {
    private String devGroupId;// 设备组ID
    private List<String> deviceIds;// 添加到设备组的设备ID列表
}
