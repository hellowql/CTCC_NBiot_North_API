package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class DeviceInfoDTO {
    @Required
    private String nodeId;// 设备的唯一标识
    private String name;// 设备名称
    private String description;// 设备的描述信息
    private String manufacturerId;// 厂商ID，唯一标识一个厂商
    private String manufacturerName;// 厂商名称
    private String mac;// 设备的MAC地址
    private String location;// 设备的位置信息
    private String deviceType;// 设备类型，大驼峰命名方式
    private String model;// 设备的型号
    private String swVersion;// 设备的软件版本
    private String fwVersion;// 设备的固件版本
    private String hwVersion;// 设备的硬件版本
    private String protocolType;// 设备使用的协议类型，当前支持的协议类型：CoAP,huaweiM2M,Z-Wave,ONVIF,WPS,Hue,WiFi,J808,Gateway,Zigbee,LWM2M
    private String bridgeId;// Bridge标识，表示设备通过哪个Bridge接入IoT平台。
    private DEVICE_STATUS status;// 设备的状态，表示设备是否在线
    private DEVICE_STATUS_DETAIL statusDetail;// 设备的状态详情
    private String mute;// 表示设备是否处于冻结状态，即设备上报数据时，平台是否会管理和保存。TRUE：冻结状态；FALSE：非冻结状态
    private String supportedSecurity;// 表示设备是否支持安全模式。TRUE：支持；FALSE：不支持
    private String isSecurity;// 表示设备当前是否启用安全模式。TRUE：启用；FALSE：未启用
    private String signalStrength;// 设备的信号强度
    private String sigVersion;// 设备的sig版本
    private String serialNumber;// 设备的序列号
    private String batteryLevel;// 设备的电池电量
}
