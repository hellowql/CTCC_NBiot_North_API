package com.dajia.hefei.nbiot.util;

import org.apache.commons.lang3.time.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateTimeUtil {

    public static String TZ2Punc(String dateTime) {
        if (null != dateTime && dateTime.matches("^\\d{8}T\\d{6}Z$")) {
            try {
                SimpleDateFormat utcFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                Date utcDate = utcFormat.parse(dateTime);
                SimpleDateFormat localFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                localFormat.setTimeZone(TimeZone.getDefault());
                dateTime = localFormat.format(utcDate.getTime());
            } catch (ParseException e) {
                e.printStackTrace();
                return dateTime;
            }
        }
        return dateTime;
    }

    public static String Punc2TZ(String dateTime) {
        if (null != dateTime && dateTime.matches("^\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}$")) {
            try {
                SimpleDateFormat localFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                localFormat.setTimeZone(TimeZone.getDefault());
                Date date = localFormat.parse(dateTime);
                SimpleDateFormat utcFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
                return utcFormat.format(date);
            } catch (ParseException e) {
                e.printStackTrace();
                return dateTime;
            }
        }
        return null;
    }

    public static String[] date2SqlBetween(String date1, String date2) {
        if (null != date1 && !date1.isEmpty() && date1.matches("\\d{4}-\\d{2}-\\d{2}.*")) {
            if (date1.length() > 10) {
                date1 = date1.substring(0, 10);
            }
            if (null != date2 && !date2.isEmpty() && date2.matches("\\d{4}-\\d{2}-\\d{2}.*")) {
                if (date2.length() > 10) {
                    date2 = date2.substring(0, 10);
                }
                return new String[]{
                        date1 + " 00:00:00",
                        date2 + " 24:00:00"
                };
            }
        }
        return null;
    }

    public static String[] dateAndHourConvert(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH");
        Calendar calendar = Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(date));
            calendar.add(Calendar.HOUR_OF_DAY, -2);
            String startDate = sdf.format(calendar.getTime());
            return new String[]{
                    startDate + ":00:00",
                    date + ":60:00"
            };
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String now() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static Date[] date2SqlBetweenYestoday() {
        return date2SqlBetweenYestoday(new Date());
    }

    public static Date[] date2SqlBetweenYestoday(Date date) {
        try {
            return new Date[]{
                    DateUtils.truncate(DateUtils.addDays(date, -1), Calendar.DAY_OF_MONTH),
                    DateUtils.truncate(date, Calendar.DAY_OF_MONTH)
            };
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String[] dateAndBeforeArr(Date date, int value, int unit) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(unit, -value);
        return new String[]{
                getDateStr(date),
                getDateStr(calendar.getTime())
        };
    }

    public static String getDateTimeStr() {
        return getDateTimeStr(new Date());
    }

    public static String getDateTimeStr(Date date) {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
    }

    public static String getLastDateTimeStr(Date dateTime) {
        return new SimpleDateFormat("yyyy-MM-dd 23:59:59").format(dateTime);
    }

    public static String getDateStr() {
        return getDateStr(new Date());
    }

    public static String getDateStr(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(date);
    }

    public static Date parseDateStr(String dateTime) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
        } catch (ParseException e) {
            try {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateTime);
            } catch (ParseException e1) {
                e1.printStackTrace();
            }
        }
        return null;
    }
}
