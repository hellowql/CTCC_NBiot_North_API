package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class ServiceCommand {
    private String commandName;// 命令名称
    private List<ServiceCommandPara> paras;// 属性列表
    private List<ServiceCommandResponse> responses;// 响应列表
}
