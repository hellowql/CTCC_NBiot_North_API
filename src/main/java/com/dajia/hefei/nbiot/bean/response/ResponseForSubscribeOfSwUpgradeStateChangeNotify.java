package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfSwUpgradeStateChangeNotify {
    @Required
    private String notifyType;// 通知类型，取值：swUpgradeStateChangeNotify
    @Required
    private String deviceId;// 设备ID
    @Required
    private String appId;
    @Required
    private String operationId;// 软件升级任务ID
    @Required
    private String subOperationId;// 软件升级子任务ID
    @Required
    private String swUpgradeState;// 软件升级状态。downloading：设备正在下载软件包；downloaded：设备下载软件包完成；updating：设备正在进行升级；idle：设备处于空闲状态
}
