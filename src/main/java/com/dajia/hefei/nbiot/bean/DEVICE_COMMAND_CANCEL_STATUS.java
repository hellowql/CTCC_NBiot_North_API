package com.dajia.hefei.nbiot.bean;

public enum DEVICE_COMMAND_CANCEL_STATUS {
    WAITTING/*表示等待执行中*/,
    RUNNING/*表示撤销任务正在执行*/,
    SUCCESS/*表示撤销任务执行成功*/,
    FAILED/*白哦是撤销任务执行失败*/,
    PART_SUCCESS/*表示撤销任务部分执行成功*/
}
