package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryDeviceFromDeviceGroup {
    @Required
    private String devGroupId;// 设备组ID，在增加设备组后由IoT平台返回获得。
    private String accessAppId;// 当查询授权应用下的设备组时需要填写，填写授权应用的应用ID
    private Integer pageNo;// 查询的页码，默认值：0
    private Integer pageSize;// 每页设备记录数量，默认值为10
}
