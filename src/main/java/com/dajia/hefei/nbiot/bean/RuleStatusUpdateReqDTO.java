package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class RuleStatusUpdateReqDTO {
    @Required
    private String ruleId;// 规则ID
    @Required
    private RULE_STATUS status;// 需要修改的规则状态
}
