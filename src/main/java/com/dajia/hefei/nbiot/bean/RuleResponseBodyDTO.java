package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class RuleResponseBodyDTO {
    private String ruleId;// 规则ID
    private Object response;// 执行结果，如果成功则为空，否则包含异常信息。ExceptionMsg
}
