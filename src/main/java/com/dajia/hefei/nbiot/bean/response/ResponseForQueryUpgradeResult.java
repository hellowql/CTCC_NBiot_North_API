package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.OperateDevices;
import com.dajia.hefei.nbiot.bean.OperatePolicy;
import com.dajia.hefei.nbiot.bean.OperationStaResult;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryUpgradeResult extends BaseResponse {
    private String operationId;// 操作任务ID
    private String createTime;// 操作任务的创建时间
    private String startTime;// 操作任务的启动时间
    private String stopTime;// 操作任务的停止时间
    private String operateType;// 操作类型
    private OperateDevices targets;// 执行操作的目标设备
    private OperatePolicy policy;// 操作执行策略
    private String status;// 操作任务的状态。wait：等待；processing：正在执行；failed：失败；success：成功；stop：停止
    private OperationStaResult staResult;// 操作结果统计
    private Object extendPara;// 操作扩展参数，视不同类型的操作不同
}
