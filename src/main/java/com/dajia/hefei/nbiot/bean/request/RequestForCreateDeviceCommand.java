package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.CommandDTOV4;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForCreateDeviceCommand {
    @Required
    private String deviceId;// 下发命令的设备ID，用于唯一标识一个设备
    @Required
    private CommandDTOV4 command;// 下发命令的信息
    private String callbackUrl;// 命令状态变化通知地址，当命令状态变化时（执行失败，执行成功，超时，发送，已送达）会通知第三方应用
    private Integer expireTime;// 下发命令的有效时间，单位为秒，表示设备命令在创建expireTime秒内有效，超过这个时间范围后命令将不再下发，如果未设置则默认未48小时。如果expireTime设置为0，则无论IoT平台上设置的命令下发模式是什么，该命令都会立即下发给设备（如果设备处于休眠状态或者链路已老化，则设备收不到命令，平台没收到设备的响应，该命令最终会超时）
    private Integer maxRetransmit;// 命令下发最大重传次数，范围0~3
}
