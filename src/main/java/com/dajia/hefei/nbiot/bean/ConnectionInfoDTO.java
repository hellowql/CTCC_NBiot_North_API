package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class ConnectionInfoDTO {
    private String connectionInfo;
}
