package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForRegisterDirectlyConnectedDevice {
    @Required
    private String nodeId;// 设备的唯一标识，通常使用MAC，MAC地址，Serial No或IMEI作为nodeId。
    private String verifyCode;// 设备验证码，若在请求中指定verifyCode，则响应中返回请求中指定的verifyCode；若请求中不指定verifyCode，则由IoT平台自动生成。建议和nodeId设置成一样。
    private String endUserId;// 终端用户ID，如手机号码，email地址。
    private String psk;// 请求中指定psk，则平台使用指定的psk；请求中不指定psk，则由平台生成psk。取值范围是a-f、A-F、0-9组成的字符串。
    private Integer timeout;// 设备验证码有效期，单位秒，默认值180s。值为0时，表示验证码则永不过期。值为大于等于0的整数时，表示在指定时间内设备进行绑定，超过时间验证码无效，同时注册的设备会被删除。
}
