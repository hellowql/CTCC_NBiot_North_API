package com.dajia.hefei.nbiot.bean;

public enum SUB_OPERATION_STATUS {
    wait/*等待*/,
    processing/*正在执行*/,
    fail/*失败*/,
    success/*成功*/,
    stop/*停止*/
}
