package com.dajia.hefei.nbiot.bean.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class BaseResponse {
    @JsonProperty("error_code")
    private String errorCode;
    @JsonProperty("error_desc")
    private String errorDesc;

    public boolean success() {
        return null == errorCode || errorCode.trim().isEmpty();
    }
}
