package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceCommandRespV4;
import com.dajia.hefei.nbiot.bean.Pagination;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDeviceCommand extends BaseResponse {
    private Pagination pagination;// 页面信息
    private List<DeviceCommandRespV4> data;// 设备命令列表
}
