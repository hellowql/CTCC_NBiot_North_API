package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForUpdateRule extends BaseResponse {
    private String ruleId;// 规则实例ID。
}
