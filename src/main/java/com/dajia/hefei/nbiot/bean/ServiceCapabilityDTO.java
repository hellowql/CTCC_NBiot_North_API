package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class ServiceCapabilityDTO {
    private String serviceId;// 服务标识
    private String serviceType;// 服务类型
    private String option;// 服务选项
    private String description;// 描述
    private List<ServiceCommand> commands;// 支持的命令名称列表
    private List<ServiceProperty> properties;// 支持的属性名称列表
}
