package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.CategoryInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryPackages extends BaseResponse {
    private Integer totalCount;
    private Integer pageNo;
    private Integer pageSize;
    private List<CategoryInfo> data;// 版本包列表信息
}
