package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForCreateDeviceCommandCancelTask {
    @Required
    private String deviceId;
}
