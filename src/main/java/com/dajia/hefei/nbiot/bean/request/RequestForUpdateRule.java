package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.RuleDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class RequestForUpdateRule extends RuleDTO {
}
