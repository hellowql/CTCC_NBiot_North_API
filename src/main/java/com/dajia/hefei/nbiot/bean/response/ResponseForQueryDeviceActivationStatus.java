package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDeviceActivationStatus extends BaseResponse {
    private String deviceId;// 设备ID，用于唯一标识一个设备。
    private Boolean activated;// 激活状态，设备是否通过验证码获取密码的状态标识。true：已激活；false：未激活
    private String name;// 设备名称
}
