package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class OperateStaResult {
    private Long total;// 操作设备总数
    private Long wait;// 等待操作的设备个数
    private Long processing;// 正在操作的设备个数
    private Long success;// 操作成功的设备个数
    private Long fail;// 操作失败的设备个数
    private Long stop;// 停止操作的设备个数
    private Long timeout;// 操作超时失败的设备个数
}
