package com.dajia.hefei.nbiot.bean;

/**
 * 设备冻结状态
 */
public enum MUTE {
    TRUE, FALSE;
}
