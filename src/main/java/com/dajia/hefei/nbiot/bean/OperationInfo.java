package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class OperationInfo {
    private String operationId;// 操作任务ID
    private String createTime;
    private String startTime;
    private String stopTime;
    private OPERATION_TYPE operationType;// 操作类型
    private OperateDevices targets;// 执行操作的目标设备
    private OperatePolicy policy;// 操作执行策略
    private SUB_OPERATION_STATUS status;// 操作任务的状态
    private OperateStaResult staResult;// 操作结果统计
    private Object extendPara;// 操作扩展参数
}
