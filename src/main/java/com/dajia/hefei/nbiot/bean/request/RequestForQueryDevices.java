package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.DEVICE_STATUS;
import com.dajia.hefei.nbiot.bean.NODE_TYPE;
import com.dajia.hefei.nbiot.bean.SORT;
import lombok.Data;

@Data
public class RequestForQueryDevices {
    private String appId;
    private String gatewayId;// 网关ID，用于标识一个网管设备
    private NODE_TYPE nodeType;// 节点类型
    private String deviceType;// 设备类型
    private Integer pageNo;// 查询的页码
    private Integer pageSize;// 查询每页信息的数量，缺省值：1
    private DEVICE_STATUS status;// 查询设备的状态
    private String startTime;// 查询注册设备信息时间在startTime之后的记录
    private String endTime;// 查询注册设备信息时间在endTime之前的记录
    private SORT sort;// 指定返回记录的排序。ASC：按注册设备的时间升序排序；DESC：按注册设备的时间降序排序
    private String select;// 指定返回记录，可取值：imsi
}
