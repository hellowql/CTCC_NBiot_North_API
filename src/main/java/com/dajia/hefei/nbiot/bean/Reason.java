package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class Reason {
    private String satisfactionTime;// 满足条件的时间
    private String type;// 规则条件的类型
    private String id;// condition的ID
    private Object info;// 不同条件类型携带不同信息
    private Object transInfo;// 事件推送时的回填信息，对应规则的condition中的transInfo

    @Data
    public static class DEVICE_DATA {// DEVICE_DATA类型的条件携带的info
        private String realValue;// 设备数据的实际值
    }
}
