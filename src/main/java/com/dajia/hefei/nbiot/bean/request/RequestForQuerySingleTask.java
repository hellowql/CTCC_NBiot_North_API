package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Optional;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQuerySingleTask {
    @Optional
    private String select;// 指定可选的返回值，可取值：tag。
    @Required
    private String taskId;// 批量任务ID，创建批量任务后获得。
}
