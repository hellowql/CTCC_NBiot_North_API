package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.OPERATION_TYPE;
import com.dajia.hefei.nbiot.bean.SUB_OPERATION_STATUS;
import lombok.Data;

@Data
public class RequestForQueryUpgradeTask {
    private OPERATION_TYPE operationType;// 操作类型
    private SUB_OPERATION_STATUS operationStatus;// 操作任务的状态
    private String deviceType;// 操作任务针对的设备类型
    private String model;// 操作任务针对的设备设备型号
    private String manufacturerName;// 操作任务针对的设备厂家名称
    private String deviceId;// 操作任务针对的设备ID
    private Integer pageNo;
    private Integer pageSize;

}
