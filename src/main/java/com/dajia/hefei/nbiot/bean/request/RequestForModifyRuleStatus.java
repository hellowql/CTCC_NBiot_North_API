package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.RULE_STATUS;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForModifyRuleStatus {
    @Required
    private String ruleId;// 规则ID
    @Required
    private RULE_STATUS status;// 规则状态。active：激活；inactive：未激活

}
