package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class ActionResult {
    @Required
    private String type;// 规则动作的类型
    private String id;// action的ID
    private String exception;// 规则引擎构造action对应的请求过程中遇到异常时携带的异常信息
    private Object result;// 动作的执行结果，对于DEVICE_CMD/SMS/EMAIL类型的动作，内容为statusCode+body
    private Object info;// 不同动作类型携带不同信息
    private Object transInfo;// 事件推送时的回填信息，对应规则的action中的transInfo

    @Data
    public static class DEVICE_CMD {
        private String requestId;// 下发异步命令时，为请求消息分配的UUID
    }
}
