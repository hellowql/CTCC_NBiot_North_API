package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQuerySub extends BaseResponse {
    private String subscriptionId;// 订阅ID，用于标识一个订阅
    private String notifyType;// 通知的类型
    private String callbackUrl;// 订阅的回调地址
}
