package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.CommandDTOV2;
import com.dajia.hefei.nbiot.bean.Optional;
import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.bean.TagDTO2;
import lombok.Data;

import java.util.List;

@Data
public class RequestForCreateTasks {
    @Required
    private String appId;
    @Required
    private Integer timeout;// 任务超时时长，单位秒，范围为10到2880。
    @Required
    private String taskName;// 任务名称，最大长度256字符。
    @Required
    private String taskType;// 任务类型，DeviceReg/DeviceCmd
    @Required
    private Object param;// 任务详细参数，根据任务类型不同，对应不同类型的参数
    @Optional
    private List<TagDTO2> tags;// 标签列表

    @Data
    public static class DeviceReg {
        @Required
        private String fileId;// 文件上传时返回的文件ID
    }

    @Data
    public static class DeviceCmd {
        @Required
        private String type;// 批量命令类型，取值范围：DeviceList/DeviceType/DeviceArea/GroupList/Broadcast。
        private List<String> deviceList;// deviceId列表，当命令类型为DeviceList时，需要填写。
        private String deviceType;// 设备类型，当命令类型为DeviceType时需要填写，其值应当与profile中定义的一致。
        private String manufacturerId;// 厂商ID，当命令类型为DeviceType时可填写，其值应当与profile中定义的一致。
        private String model;// 设备型号，当面锣类型为DeviceType时可填写，其值应当与profile中定义的一致。
        private String deviceLocation;// 设备位置，当命令类型为DeviceArea时需要填写。
        private List<String> groupList;// 设备组名字列表，当命令类型为GroupList时需要填写。
        @Required
        private CommandDTOV2 command;// 命令
        private String callbackUrl;// 命令执行结果的回调地址。
        private Integer maxRetransmit;// 命令下发最大重传次数，取值范围0-3。
    }
}
