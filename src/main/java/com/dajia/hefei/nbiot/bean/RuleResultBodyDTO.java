package com.dajia.hefei.nbiot.bean;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RuleResultBodyDTO {
    @JsonProperty("result_code")
    private String resultCode;// 总体执行结果代码，若执行成功则未“OK”，否则未“Partial OK”
    @JsonProperty("result_desc")
    private String resultDesc;// 总体执行结果描述，若全部执行成功则为“Operation successful”，否则为“Operation partial ok”
}
