package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfDeviceModelDeleted {
    @Required
    private String notifyType;// 通知类型，取值：deviceModelDeleted
    @Required
    private String appId;
    @Required
    private String deviceType;// 设备的类型
    @Required
    private String manufacturerName;// 增加设备模型的操作者名称
    @Required
    private String manufacturerId;// 增加设备模型的操作者ID
    @Required
    private String model;// 设备型号
    @Required
    private String protocolType;// 设备使用的协议类型
}
