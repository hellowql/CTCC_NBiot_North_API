package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.SubscriptionDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQuerySubs extends BaseResponse {
    private Long totalCount;// 查询到的订阅记录总数
    private Long pageNo;// 查询的页码
    private Long pageSize;// 查询每页信息的数量
    private List<SubscriptionDTO> subscriptions;// 订阅信息列表
}
