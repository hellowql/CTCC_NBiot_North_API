package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.TagDTO2;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQuerySingleTask extends BaseResponse {
    private String taskId;// 批量任务ID
    private String taskName;// 批量任务名称
    private String appId;// 批量任务所归属的appId
    private String operator;// 下发该批量任务的操作员
    private String taskFrom;// 批量任务的来源：Portal：通过SP Portal创建的；Northbound：调用北向API接口创建的。
    private String taskType;// 批量任务的类型，取值范围：DeviceReg/DeviceCmd
    private String status;// 批量任务的状态，取值范围：Pending/Running/Complete/Timeout。
    private String startTime;// 批量任务的创建时间
    private Integer timeout;// 批量任务的超时时间，单位秒。
    private Integer progress;// 批量任务的进度，单位：千分比，范围：0-1000，向下取整。
    private Integer totalCnt;// 任务的详情总数
    private Integer successCnt;// 成功的让你无详情数
    private Integer failCnt;// 失败的任务详情数
    private Integer timeoutCnt;// 超时的任务详情数
    private Integer expiredCnt;// 未执行的失效任务详情数
    private Integer completeCnt;// 完成的任务详情数，完成的任务详情数=成功的任务详情数+失败的任务详情数+超时的任务详情数
    private Integer successRate;// 任务成功率，单位：千分比，范围：0-1000，向下取整。
    private Object param;// 不同任务类型的具体参数不同。
    private List<TagDTO2> tags;// 批量任务的标签列表
}
