package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.NOTIFYTYPE;
import com.dajia.hefei.nbiot.bean.Optional;
import lombok.Data;

@Data
public class RequestForQuerySubs {
    @Optional
    private NOTIFYTYPE notifytype;// 通知类型
    @Optional
    private Integer pageNo;// 分页查询参数，值为空时，查询内容不分页；值大于等于0的时候分页查询。
    @Optional
    private Integer pageSize;// 分页查询参数，取值大于等于1的整数，缺省值：10

}
