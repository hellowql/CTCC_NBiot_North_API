package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.FILE_TYPE;
import lombok.Data;

@Data
public class RequestForQueryPackages {
    private FILE_TYPE fileType;// 版本包类型
    private String deviceType;// 版本包适用的设备类型
    private String model;// 版本包适用的设备型号
    private String manufacturerName;// 版本包适用的设备厂商名称
    private String versioin;// 版本包的版本号
    private Integer pageNo;// 分页查询参数。
    private Integer pageSize;// 查询结果分页时的每页结果数量，取值范围1~100，默认值10


}
