package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.OperationInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryUpgradeTask extends BaseResponse {
    private Integer totalCount;
    private Integer pageSize;
    private Integer pageNo;
    private List<OperationInfo> data;// 任务列表信息
}
