package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDeviceFromDeviceGroup extends BaseResponse {
    private Long totalCount;// 设备组总数
    private Long pageNo;// 查询结果页码
    private Long pageSize;// 每页设备组记录数量
    private List<String> deviceIds;// 设备组内设备ID列表
}
