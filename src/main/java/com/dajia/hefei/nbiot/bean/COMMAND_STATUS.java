package com.dajia.hefei.nbiot.bean;

public enum COMMAND_STATUS {
    DEFAULT/*表示未下发*/,
    EXPIRED/*表示命令已经过期*/,
    SUCCESSFUL/*表示命令已成功执行*/,
    FAILED/*表示命令执行失败*/,
    TIMEOUT/*表示命令下发执行超时*/,
    CANCELED/*表示命令已经被撤销执行*/,
    DELIVERED/*交付*/
}
