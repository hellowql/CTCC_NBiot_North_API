package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryAccessToken extends BaseResponse {
    private String accessToken;// 鉴权参数，访问IoT平台API接口的凭证。
    private String tokenType;// accessToken的类型，参数值固定为bearer。
    private String refreshToken;// 鉴权参数，用来刷新accessToken，refreshToken的有效时间为1个月。
    private Integer expiresIn;// 平台生成并返回accessToken的有效时间，单位为秒。
    private String scope;// 申请权限范围，即accessToken所能访问IoT平台资源的范围，参数值固定为default。
}
