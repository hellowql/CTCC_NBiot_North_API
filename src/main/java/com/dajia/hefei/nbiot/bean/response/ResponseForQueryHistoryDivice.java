package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceDataHistoryDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryHistoryDivice extends BaseResponse {
    private List<DeviceDataHistoryDTO> deviceDataHistoryDTOs;// 设备历史数据列表
}
