package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceCapabilityDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDeviceCapabilities extends BaseResponse {
    private List<DeviceCapabilityDTO> deviceCapabilities;
}
