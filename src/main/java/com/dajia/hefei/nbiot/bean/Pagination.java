package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class Pagination {
    private Integer pageNo;
    private Integer pageSize;
    private Integer totalSize;
}
