package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

import java.util.List;

@Data
public class RequestForAddDeviceToGroup {
    @Required
    private List<String> deviceIds;// 要添加到设备组的设备ID列表
    private String accessAppId;// 当修改授权应用下的设备组时需要填写，填写授权应用的应用ID
    @Required
    private String devGroupId;// 设备组ID
}
