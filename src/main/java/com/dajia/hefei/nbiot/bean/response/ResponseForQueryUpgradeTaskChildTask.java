package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.SubOperationInfo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryUpgradeTaskChildTask extends BaseResponse {
    private Long totalSize;
    private Long pageSize;
    private Long pageNo;
    private List<SubOperationInfo> data;// 子任务列表信息
}
