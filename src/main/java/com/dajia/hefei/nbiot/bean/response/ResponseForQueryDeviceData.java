package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.ConnectionInfoDTO;
import com.dajia.hefei.nbiot.bean.DeviceInfoDTO;
import com.dajia.hefei.nbiot.bean.ServiceDTO;
import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDeviceData extends BaseResponse {
    private String deviceId;
    private String gatewayId;
    private String nodeType;
    private String createTime;
    private String lastModifiedTime;
    private DeviceInfoDTO deviceInfo;
    private List<ServiceDTO> services;
    private ConnectionInfoDTO connectionInfo;
    //TODO 暂时不知道结构
    private List<Object> devGroupIds;

    public String getCreateTime() {
        return DateTimeUtil.TZ2Punc(createTime);
    }

    public String getLastModifiedTime() {
        return DateTimeUtil.TZ2Punc(lastModifiedTime);
    }
}
