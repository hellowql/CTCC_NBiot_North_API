package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class OperationStaResult {
    private Integer total;// 操作设备总数
    private Integer wait;// 等待操作的设备个数
    private Integer processing;// 正在操作的设备个数
    private Integer success;// 操作成功的设备个数
    private Integer fail;// 操作失败的设备个数
    private Integer stop;// 停止操作的设备个数
    private Integer timeout;// 操作超时失败的设备个数
}
