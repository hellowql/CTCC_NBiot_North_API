package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.bean.ServiceDTO;
import lombok.Data;

@Data
public class ResponseForSubscribeOfServiceInfoChanged {
    @Required
    private String notifyType;// 通知类型，取值：serviceInfoChanged
    @Required
    private String deviceId;// 设备ID
    @Required
    private String gatewayId;// 网关ID
    @Required
    private String serviceId;// 设备服务标识
    @Required
    private String serviceType;// 设备服务类型
    @Required
    private ServiceDTO.ServiceInfo serviceInfo;// 设备服务信息，增量上报
}
