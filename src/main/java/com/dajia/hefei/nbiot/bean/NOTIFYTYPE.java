package com.dajia.hefei.nbiot.bean;

public enum NOTIFYTYPE {
    // 业务数据类型
    deviceAdded/*添加新设备*/,
    deviceInfoChanged/*设备信息变化*/,
    deviceDataChanged/*设备数据变化*/,
    deviceDatasChanged/*设备数据批量变化*/,
    deviceDeleted/*删除设备*/,
    messageConfirm/*消息确认*/,
    commandRsp/*命令响应*/,
    deviceEvent/*设备事件*/,
    serviceInfoChanged/*服务信息变化*/,
    ruleEvent/*规则事件*/,
    deviceModelAdded/*添加设备模型*/,
    deviceModelDeleted/*删除设备模型*/,

    // 管理数据类型
    swUpgradeStateChangeNotify/*软件升级状态变化通知*/,
    swUpgradeResultNotify/*软件升级结果通知*/,
    fwUpgradeStateChangeNotify/*硬件升级状态变化通知*/,
    fwUpgradeResultNotify/*硬件升级结果通知*/,
}
