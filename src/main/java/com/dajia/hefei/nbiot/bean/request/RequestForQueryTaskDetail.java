package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryTaskDetail {
    @Required
    private String taskId;// 批量任务的ID
    private String status;// 任务详情的状态：Pending/Success/Fail/Timeout。
    private Integer index;// 批量查询文件里第几行的任务，查询批量注册任务时使用。
    private String nodeId;// 设备nodeId，查询批量注册任务时使用。
    private String deviceId;// 设备Id，查询批量命令任务时使用。
    private String commandId;// 命令Id，查询批量命令任务时使用。
    private Integer pageNo;// 分页查询参数：值为空时，不分页；值大于等于0时，分页查询；值为0时查询第一页。
    private Integer pageSize;// 分页查询参数，取值大于等于1的整数，缺省值：1
}
