package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.DEVICE_COMMAND_CANCEL_STATUS;
import com.dajia.hefei.nbiot.bean.Optional;
import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

@Data
public class RequestForQueryDeviceCommandCancelTask {
    @Optional
    private Integer pageNo;// 查询的页码，默认值：0
    @Optional
    private Integer pageSize;// 查询每页信息的数量，取值范围1~1000，默认值：1000
    @Optional
    private String taskId;// 撤销任务的任务ID
    @Optional
    private String deviceId;// 执行设备命令撤销任务的设备ID
    @Optional
    private DEVICE_COMMAND_CANCEL_STATUS status;// 设备命令撤销任务的状态
    @Optional
    private String startTime;// 查询开始时间，查询创建撤销设备命令任务时间在startTime之后的记录。时间格式：yyyyMMdd'T'HHmmss'Z'
    @Optional
    private String endTime;// 查询结束时间，查询创建撤销设备命令任务时间在endTime之前的记录。时间格式：yyyyMMdd'T'HHmmss'Z'

    public void setStartTime(String startTime) {
        this.startTime = DateTimeUtil.Punc2TZ(startTime);
    }

    public void setEndTime(String endTime) {
        this.endTime = DateTimeUtil.Punc2TZ(endTime);
    }
}
