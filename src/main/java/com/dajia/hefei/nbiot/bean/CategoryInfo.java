package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class CategoryInfo {
    private String fileId;// 版本包ID
    private String name;// 版本包名称
    private String version;// 版本包版本号
    private FILE_TYPE fileType;// 版本包文件类型
    private String deviceType;// 版本包适用的设备类型
    private String model;// 版本包适用的设备型号
    private String manufacturerName;// 版本包适用的设备厂商名称
    private String protocolType;// 版本包适用的设备协议类型
    private String description;// 版本包的描述信息
    private String date;// 版本包的生成时间
    private String uploadTime;// 版本包的上传日期
}
