package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class OperatePolicy {
    @Required
    private EXECUTE_TYPE executeType;// 执行类型，默认值为100。
    private String startTime;// 任务执行时间，executeType=custom时必选
    private String endTime;//  任务停止时间，executeType=command时必选
    private boolean retryType;// 执行失败是否进行重试，默认不重试
    private Integer retryTimes;// 重试次数，取值范围1-5
}
