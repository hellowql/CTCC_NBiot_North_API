package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class DeviceConfigDTO {
    private DataConfigDTO dataConfig;
}
