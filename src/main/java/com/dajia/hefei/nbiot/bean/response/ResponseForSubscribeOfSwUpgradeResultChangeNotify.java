package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfSwUpgradeResultChangeNotify {
    @Required
    private String notifyType;// 通知类型，取值：swUpgradeResultNotify
    @Required
    private String deviceId;// 设备ID
    @Required
    private String appId;
    @Required
    private String operationId;// 软件升级任务ID
    @Required
    private String subOperationId;// 软件升级子任务ID
    @Required
    private String curVersion;// 设备当前的软件版本
    @Required
    private String targetVersion;// 设备要升级的目标软件版本
    @Required
    private String sourceVersion;// 设备的源软件版本
    @Required
    private String swUpgradeResult;// 软件升级结果。SUCCESS：设备升级成功；FAIL：设备升级失败
    @Required
    private String upgradeTime;// 升级时长
    @Required
    private String resultDesc;// 升级结果描述
    @Required
    private String errorCode;// 设备上报的状态错误码
    @Required
    private String description;// 错误原因描述

}
