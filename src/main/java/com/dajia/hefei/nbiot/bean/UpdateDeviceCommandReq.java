package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class UpdateDeviceCommandReq {
    @Required
    private final String status = "CANCELED";// 命令执行结果，可选值：CANCELED，撤销命令
}
