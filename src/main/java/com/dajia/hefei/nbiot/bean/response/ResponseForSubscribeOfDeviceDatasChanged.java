package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceServiceData;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

import java.util.List;

@Data
public class ResponseForSubscribeOfDeviceDatasChanged {
    @Required
    private String notifyType;// 通知类型，取值：deviceDatasChanged
    @Required
    private String requestId;// 消息的序列号，唯一标识该消息
    @Required
    private String deviceId;// 设备ID
    @Required
    private String gatewayId;// 网关ID
    @Required
    private List<DeviceServiceData> services;// 设备的服务数据
}
