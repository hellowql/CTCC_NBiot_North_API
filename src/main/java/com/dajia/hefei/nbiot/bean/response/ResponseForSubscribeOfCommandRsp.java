package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfCommandRsp {
    @Required
    private String notifyType;// 通知类型，取值：commandRsp
    @Required
    private Header header;
    @Required
    private Object body;// 响应命令的消息内容

    @Data
    public static class Header {
        @Required
        private String requestId;// 消息的序列号，唯一标识该消息
        @Required
        private String from;// 表示消息发布者的地址。设备发起的请求：/devices/{deviceId}；设备服务发起的请求：/devices/{deviceId}/services/{serviceId}
        @Required
        private String to;// 表示消息接收者的地址，To就是订阅请求中的From，如第三方原因的userId
        @Required
        private String deviceId;// 设备ID
        @Required
        private String serviceType;// 命令所属的服务类型
        @Required
        private String method;// 存放的响应命令，如：INVITE-RSP
    }
}
