package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceInfoDTO;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfDeviceInfoChanged {
    @Required
    private String notifyType;// 通知类型，取值：deviceInfoChanged
    @Required
    private String deviceId;// 设备ID
    @Required
    private String gatewayId;// 网关ID
    @Required
    private String nodeType;// 设备类型
    @Required
    private DeviceInfoDTO deviceInfo;
}
