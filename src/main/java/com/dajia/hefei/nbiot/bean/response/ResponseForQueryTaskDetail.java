package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.QueryTaskDetailDTOCloud2NA;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryTaskDetail extends BaseResponse{
    private Integer pageNo;
    private Integer pageSize;
    private Integer totalCount;
    private List<QueryTaskDetailDTOCloud2NA> taskDetails;
}
