package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.bean.RuleStatusUpdateReqDTO;
import lombok.Data;

import java.util.List;

@Data
public class RequestForModifyRulesStatus {
    @Required
    private List<RuleStatusUpdateReqDTO> requests;// 请求结构体列表
}
