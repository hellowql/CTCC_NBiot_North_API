package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.ActionResult;
import com.dajia.hefei.nbiot.bean.Reason;
import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

import java.util.List;

@Data
public class ResponseForSubscribeOfRuleEvent {
    @Required
    private String notifyType;// 通知类型，取值：ruleEvent
    @Required
    private String author;// 创建此规则的用户的ID
    private String ruleId;// 规则实例的ID
    private String ruleName;// 规则实例的名称
    private String logic;// 多条件逻辑关系
    private List<Reason> reasons;// 触发原因，对应conditions
    private String triggerTime;// 规则触发的时间
    private List<ActionResult> actionResults;// 规则动作执行的结果


}
