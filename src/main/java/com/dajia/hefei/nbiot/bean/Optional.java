package com.dajia.hefei.nbiot.bean;

import java.lang.annotation.*;

@Inherited
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface Optional {
    boolean value() default true;
}
