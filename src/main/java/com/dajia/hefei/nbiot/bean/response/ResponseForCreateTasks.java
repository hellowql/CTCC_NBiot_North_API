package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForCreateTasks extends BaseResponse {
    private String taskID;// 批量任务的ID
}
