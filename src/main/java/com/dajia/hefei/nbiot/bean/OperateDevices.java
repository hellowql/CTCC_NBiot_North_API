package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class OperateDevices {
    private List<String> deviceGroups;
    private String deviceType;
    private String model;
    private String manufacturerName;
    private List<String> devices;
}
