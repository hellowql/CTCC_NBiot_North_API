package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class CommandDTOV2 {
    @Required
    private String serviceId;// 命令对应的服务ID，要与profile中定义的serviceId保持一致。
    @Required
    private String method;// 服务下具体的命令名称，要与profile中定义的命令名保持一致。
    @Optional
    private TagDTO2 paras;// 命令参数，jsonString格式，json体中的键为服务profile里定义命令的参数名。
}
