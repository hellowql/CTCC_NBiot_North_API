package com.dajia.hefei.nbiot.bean.request;

import lombok.Data;

@Data
public class RequestForQueryDeviceGroups {
    private String accessAppId;// 当查询授权应用下的设备组时需要填写，填写授权应用的应用ID
    private Integer pageNo;// 分页查询参数
    private Integer pageSize;// 每页设备组记录数量，默认值为1
    private String name;// 设备组名称

}
