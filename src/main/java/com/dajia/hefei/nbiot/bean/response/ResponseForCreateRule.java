package com.dajia.hefei.nbiot.bean.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForCreateRule extends BaseResponse {
    private String ruleId;// 规则实例ID。
}
