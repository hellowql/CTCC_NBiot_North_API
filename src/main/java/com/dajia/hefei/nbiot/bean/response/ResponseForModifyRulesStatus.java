package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.RuleResponseBodyDTO;
import com.dajia.hefei.nbiot.bean.RuleResultBodyDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForModifyRulesStatus extends BaseResponse {
    private RuleResultBodyDTO result;// 总体执行结果
    private List<RuleResponseBodyDTO> responses;// 每个条目的执行结果
}
