package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.bean.UpdateDeviceCommandReq;
import lombok.Data;

@Data
public class RequestForModifyDeviceCommand {
    @Required
    private String deviceCommandId;// 要修改的命令ID，在调用创建设备命令接口后获得
    @Required
    private UpdateDeviceCommandReq body;// 修改设备命令的内容
}
