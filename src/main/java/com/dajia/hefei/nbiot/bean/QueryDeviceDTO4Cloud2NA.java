package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class QueryDeviceDTO4Cloud2NA {
    private String deviceId;
    private String gatewayId;
    private String nodeType;
    private String createTime;
    private String lastModifiedTime;
    private DeviceInfoDTO deviceInfo;
    private List<ServiceDTO> services;
}
