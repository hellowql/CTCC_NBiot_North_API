package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

import java.util.List;

@Data
public class RequestForCreateDeviceGroup {
    @Required
    private String name;// 设备组名称，仅限大小写字母和数字
    private String description;// 设备组的描述信息
    private String appId;
    private Integer maxDevNum;// 设备组设备最大数量，默认最小值为0,。当值为0时，表示对设备数量不做限制。
    private List<String> deviceIds;// 添加到设备组的设备ID列表。
}
