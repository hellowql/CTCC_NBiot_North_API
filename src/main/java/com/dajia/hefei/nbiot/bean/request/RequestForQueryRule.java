package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class RequestForQueryRule {
    @Required
    private String author;// 创建规则的用户
    private String name;// 规则名称
    private String select;// 指定可选的返回值，可取值：tag
}
