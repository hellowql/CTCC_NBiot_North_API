package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import lombok.Data;

@Data
public class ResponseForSubscribeOfFwUpgradeStateChangeNotify {
    @Required
    private String notifyType;// 通知类型，取值：fwUpgradeStateChangeNotify
    @Required
    private String deviceId;// 设备ID
    @Required
    private String appId;
    @Required
    private String operationId;// 软件升级任务ID
    @Required
    private String subOperationId;// 软件升级子任务ID
    @Required
    private String step;// 固件升级状态，可取值为：0，1，2，3.
    @Required
    private String stepDesc;// 升级状态描述。0：idle：设备处于空闲状态；1：downloading：设备正在现在软件包；2：downloaded：设备下载软件包完成；3：updating：设备正在进行升级
}
