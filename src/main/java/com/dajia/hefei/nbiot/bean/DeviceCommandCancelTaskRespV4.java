package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class DeviceCommandCancelTaskRespV4 {
    private String taskId;// 设备命令撤销任务的任务ID
    private String appId;// 设备命令撤销任务所属的应用ID
    private String deviceId;// 设备命令撤销任务指定撤销命令的设备ID
    private DEVICE_COMMAND_CANCEL_STATUS status;// 撤销任务的任务状态
    private Integer totalCount;// 撤销的设备命令总数
    private List<DeviceCommandRespV4> deviceCommands;// 设备命令撤销任务信息列表
}
