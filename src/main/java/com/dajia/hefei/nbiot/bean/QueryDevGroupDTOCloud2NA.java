package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class QueryDevGroupDTOCloud2NA {
    private String name;// 设备组名称，仅限大小写字母和数字
    private String description;// 设备组的描述信息
    private String id;// 设备组ID
    private String appId;
    private Integer maxDevNum;// 设备组设备最大数量限制，当值为0时，表示对设备数量不做限制。
    private Integer curDevNum;// 当前设备组内的设备数量
    private String creator;// 创建设备组的用户名称
}
