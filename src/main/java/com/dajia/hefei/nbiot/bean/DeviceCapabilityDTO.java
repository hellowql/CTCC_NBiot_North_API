package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class DeviceCapabilityDTO {
    private String deviceId;
    private List<ServiceCapabilityDTO> serviceCapabilities;
}
