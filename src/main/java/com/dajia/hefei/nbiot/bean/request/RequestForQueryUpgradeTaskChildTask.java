package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.SUB_OPERATION_STATUS;
import lombok.Data;

@Data
public class RequestForQueryUpgradeTaskChildTask {
    private SUB_OPERATION_STATUS subOperationStatus;// 子任务状态，不指定，则查询该任务下所有子任务执行详情
    private Integer pageNo;
    private Integer pageSize;

}
