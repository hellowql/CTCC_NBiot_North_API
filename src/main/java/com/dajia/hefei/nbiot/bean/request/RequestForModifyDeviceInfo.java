package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.DeviceConfigDTO;
import com.dajia.hefei.nbiot.bean.MUTE;
import lombok.Data;

@Data
public class RequestForModifyDeviceInfo {
    private String name;// 设备名称
    private String endUser;// 终端用户，若设备为网关，则endUser可选；若设备是通过网关接入的，则endUser必须为null。
    private MUTE mute;// 表示设备是否处于冻结状态，即设备上报数据时，平台是否会管理和保存。TRUE：冻结状态；FALSE：非冻结状态
    private String manufacturerId;// 厂商ID，唯一标识一个厂商
    private String manufacturerName;// 厂商名称
    private String deviceType;// 设备类型: 大驼峰命名方式 MultiSensor,ContactSensor,Camera,WaterMeter。在NB-IoT方案中，注册设备后必须修改设备类型，且要与profile中定义的保持一致。
    private String model;// 设备型号，由厂商定义
    private String location;// 设备的位置
    private String protocolType;// 设备使用的协议类型，当前支持的协议类型：CoAP，huaweiM2M，Z-Wave，ONVIF，WPS，Hue，WiFi，J808，Gateway，ZigBee，LWM2M。
    private DeviceConfigDTO deviceConfig;// 设备配置信息，自定义结构体
    private String region;// 设备区域信息
    private String organization;// 设备所属的组织信息
    private String timezone;// 设备所在时区信息，使用时区编码，如北京时区对应的时区编码为Asia/Beijing。
    private String imsi;// NB-IoT终端的IMSI。

}
