package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DEVICE_COMMAND_CANCEL_STATUS;
import com.dajia.hefei.nbiot.bean.DeviceCommandRespV4;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForCreateDeviceCommandCancelTask extends BaseResponse {
    private String taskId;// 设备命令撤销任务的任务ID
    private String appId;
    private String deviceId;// 执行设备命令撤销任务的设备ID
    private DEVICE_COMMAND_CANCEL_STATUS status;// 撤销任务的任务状态
    private Integer totalCount;// 撤销的设备命令总数
    private List<DeviceCommandRespV4> deviceCommands;// 撤销的设备命令列表
}
