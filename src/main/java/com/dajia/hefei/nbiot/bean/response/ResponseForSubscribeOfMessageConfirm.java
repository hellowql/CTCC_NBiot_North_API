package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.Required;
import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

@Data
public class ResponseForSubscribeOfMessageConfirm {
    @Required
    private String notifyType;// 通知类型，取值：messageConfirm
    @Required
    private Header header;
    @Required
    private Object body;// 根据业务具体定义，确认消息可以携带的状态变化等消息


    @Data
    public static class Header {
        @Required
        private String requestId;// 消息的序列号，唯一标识该消息
        @Required
        private String from;// 表示消息发布者的地址。设备发起的请求：/devices/{deviceId}；设备服务发起的请求：/devices/{deviceId}/services/{serviceId}
        @Required
        private String to;// 表示消息接收者的地址，To就是订阅请求中的From，如第三方原因的userId
        @Required
        private String status;// 命令状态。sent：已发送；delivered：已送达；executed：已执行
        @Required
        private String timestamp;// 时间戳

        public String getTimestamp() {
            return DateTimeUtil.TZ2Punc(this.timestamp);
        }
    }
}
