package com.dajia.hefei.nbiot.util;

public class Constant {
    public static final String BASE_URL = "https://180.101.147.89:8743";

    // 北京
    public static final String APPID = "";
    public static final String SECRET = "";

    public static final String DEVICE_ID = "19ecdc22-0eeb-4890-86fc-1468ff2f7c59";

    public static final String GATEWAY_ID = DEVICE_ID;

//    public static final String CALLBACK_BASE_URL = "http://192.88.88.88:9999";
//    public static final String DEVICE_ADDED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/addDevice";
//    public static final String DEVICE_INFO_CHANGED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/updateDeviceInfo";
//    public static final String DEVICE_DATA_CHANGED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/updateDeviceData";
//    public static final String DEVICE_DELETED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/deletedDevice";
//    public static final String MESSAGE_CONFIRM_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/commandConfirmData";
//    public static final String SERVICE_INFO_CHANGED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/updateServiceInfo";
//    public static final String COMMAND_RSP_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/commandRspData";
//    public static final String DEVICE_EVENT_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/DeviceEvent";
//    public static final String RULE_EVENT_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/RulEevent";
//    public static final String DEVICE_DATAS_CHANGED_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/updateDeviceDatas";
//    public static final String REPORT_CMD_EXEC_RESULT_CALLBACK_URL = CALLBACK_BASE_URL + "/na/iocm/devNotify/v1.1.0/reportCmdExecResult";

    public static final String HEADER_APP_KEY = "app_key";
    public static final String HEADER_APP_AUTH = "Authorization";

    /*
     * Application Access Security:
     * 1. APP_AUTH
     * 2. REFRESH_TOKEN
     */
    public static final String APP_AUTH = BASE_URL + "/iocm/app/sec/v1.1.0/login";
    public static final String REFRESH_TOKEN = BASE_URL + "/iocm/app/sec/v1.1.0/refreshToken";


    /*
     * Device Management:
     * 1. REGISTER_DEVICE
     * 2. MODIFY_DEVICE_INFO
     * 3. QUERY_DEVICE_ACTIVATION_STATUS
     * 4. DELETE_DEVICE
     * 5. DISCOVER_INDIRECT_DEVICE
     * 6. REMOVE_INDIRECT_DEVICE
     */
    public static final String REGISTER_DEVICE = BASE_URL + "/iocm/app/reg/v1.1.0/deviceCredentials";
    public static final String MODIFY_DEVICE_INFO = BASE_URL + "/iocm/app/dm/v1.4.0/devices/";
    public static final String QUERY_DEVICE_ACTIVATION_STATUS = BASE_URL + "/iocm/app/reg/v1.1.0/deviceCredentials/";
    public static final String DELETE_DEVICE = BASE_URL + "/iocm/app/dm/v1.4.0/devices/";

    /*
     * Data Collection:
     * 1. QUERY_DEVICES
     * 2. QUERY_DEVICE_DATA
     * 3. QUERY_DEVICE_HISTORY_DATA
     * 4. QUERY_DEVICE_CAPABILITIES
     * 5. SUBSCRIBE_NOTIFYCATION
     */
    public static final String QUERY_DEVICES = BASE_URL + "/iocm/app/dm/v1.4.0/devices";
    public static final String QUERY_DEVICE_HISTORY_DATA = BASE_URL + "/iocm/app/data/v1.2.0/deviceDataHistory";
    public static final String QUERY_DEVICE_CAPABILITIES = BASE_URL + "/iocm/app/data/v1.1.0/deviceCapabilities";


    /*
     * Signaling Delivery：
     * 1. POST_ASYN_CMD
     * 2. QUERY_DEVICE_CMD
     * 3. UPDATE_ASYN_COMMAND
     * 4. CREATE_DEVICECMD_CANCEL_TASK
     * 5. QUERY_DEVICECMD_CANCEL_TASK
     *
     */
    public static final String POST_ASYN_CMD = BASE_URL + "/iocm/app/cmd/v1.4.0/deviceCommands";
    public static final String CREATE_DEVICECMD_CANCEL_TASK = BASE_URL + "/iocm/app/cmd/v1.4.0/deviceCommandCancelTasks";


    /*
     * Batchtask
     */
    public static final String BATCHTASK = BASE_URL + "/iocm/app/batchtask/v1.1.0/tasks";
    public static final String BATCHTASK_DETAIL = BASE_URL + "/iocm/app/batchtask/v1.1.0/taskDetails";

    /*
     * Rules
     */
    public static final String RULES = BASE_URL + "/iocm/app/rule/v1.2.0/rules";
    public static final String BATCH_MODIFY_RULES = BASE_URL + "/iocm/app/rule/v1.2.0/rules/status";
    public static final String MODIFY_RULES = BASE_URL + "/iocm/app/rule/v1.2.0/rules/%s/status/%s";

    /*
     * sub
     */
    public static final String SUB_DATA = BASE_URL + "/iocm/app/sub/v1.2.0/subscriptions";
    public static final String SUB_MANAGE = BASE_URL + "/iodm/app/sub/v1.1.0/subscribe";

    /*
     *devGroups
     */
    public static final String DEV_GROUP = BASE_URL + "/iocm/app/devgroup/v1.3.0/devGroups";
    public static final String DEV_GROUP_DEVICE = BASE_URL + "/iocm/app/dm/v1.2.0/devices/ids";
    public static final String DEV_GROUP_ADD_DEVICE = BASE_URL + "/iocm/app/dm/v1.1.0/devices/addDevGroupTagToDevices";
    public static final String DEV_GROUP_DELETE_DEVICE = BASE_URL + "/iocm/app/dm/v1.1.0/devices/deleteDevGroupTagFromDevices";

    /*
     *package
     */
    public static final String PACKAGE = BASE_URL + "/iodm/northbound/v1.5.0/category";
    public static final String SOFTWARE_UPGRADE = BASE_URL + "/iodm/northbound/v1.5.0/operatioins/softwareUpgrade";
    public static final String SOFTWARE_UPGRADE_RESULT = BASE_URL + "/iodm/northbound/v1.5.0/operatioins";
    public static final String SOFTWARE_UPGRADE_CHILD_TASK = BASE_URL + "/iodm/northbound/v1.5.0/operatioins/%s/subOperations";


    /*
     * notify Type
     * serviceInfoChanged|deviceInfoChanged|LocationChanged|deviceDataChanged|deviceDatasChanged
     * deviceAdded|deviceDeleted|messageConfirm|commandRsp|deviceEvent|ruleEvent
     */
    public static final String DEVICE_ADDED = "deviceAdded";
    public static final String DEVICE_INFO_CHANGED = "deviceInfoChanged";
    public static final String DEVICE_DATA_CHANGED = "deviceDataChanged";
    public static final String DEVICE_DELETED = "deviceDeleted";
    public static final String MESSAGE_CONFIRM = "messageConfirm";
    public static final String SERVICE_INFO_CHANGED = "serviceInfoChanged";
    public static final String COMMAND_RSP = "commandRsp";
    public static final String DEVICE_EVENT = "deviceEvent";
    public static final String RULE_EVENT = "ruleEvent";
    public static final String DEVICE_DATAS_CHANGED = "deviceDatasChanged";

}
