package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.QueryDeviceDTO4Cloud2NA;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDevices extends BaseResponse {
    private Long totalCount;
    private Long pageNo;
    private Long pageSize;
    private List<QueryDeviceDTO4Cloud2NA> devices;
}
