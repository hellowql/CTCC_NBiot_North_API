package com.dajia.hefei.nbiot.bean.response;

import com.dajia.hefei.nbiot.bean.DeviceInfoDTO;
import com.dajia.hefei.nbiot.bean.NODE_TYPE;
import com.dajia.hefei.nbiot.bean.ServiceDTO;
import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
public class ResponseForQueryDevice extends BaseResponse {
    private String deviceId;// 设备ID，用于唯一标识一个设备
    private String gatewayId;// 网关ID，用于标识一个网关设备
    private NODE_TYPE nodeType;// 节点类型
    private String createTime;// 创建设备的时间
    private String lastModifiedTime;// 最后修改设备的时间
    private DeviceInfoDTO deviceInfo;// 设备信息
    private List<ServiceDTO> services;// 设备服务列表

    public String getCreateTime() {
        return DateTimeUtil.TZ2Punc(this.createTime);
    }
}
