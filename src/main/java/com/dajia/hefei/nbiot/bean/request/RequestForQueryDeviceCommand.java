package com.dajia.hefei.nbiot.bean.request;

import com.dajia.hefei.nbiot.bean.Optional;
import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

@Data
public class RequestForQueryDeviceCommand {
    @Optional
    private Integer pageNo;// 查询的页码，默认值：0
    @Optional
    private Integer pageSize;// 查询每页信息的数量，取值范围1~1000，默认值：1000
    @Optional
    private String deviceId;// 指定查询命令的设备ID
    @Optional
    private String startTime;// 查询开始时间，查询下发命令时间在startTime之后的记录。时间格式：yyyyMMdd'T'HHmmss'Z'
    @Optional
    private String endTime;// 查询结束时间，查询下发命令时间在endTime之前的记录。时间格式：yyyyMMdd'T'HHmmss'Z'

    public void setStartTime(String startTime) {
        this.startTime = DateTimeUtil.Punc2TZ(startTime);
    }

    public void setEndTime(String endTime) {
        this.endTime = DateTimeUtil.Punc2TZ(endTime);
    }
}
