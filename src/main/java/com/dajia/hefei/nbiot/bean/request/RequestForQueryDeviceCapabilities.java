package com.dajia.hefei.nbiot.bean.request;

import lombok.Data;

@Data
public class RequestForQueryDeviceCapabilities {
    private String deviceId;
    private String gatewayId;
}
