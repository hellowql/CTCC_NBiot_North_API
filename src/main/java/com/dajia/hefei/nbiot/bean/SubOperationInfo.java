package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class SubOperationInfo {
    private String subOperationId;// 子任务ID
    private String createTime;// 子任务创建时间
    private String startTime;// 子任务启动时间
    private String stopTime;// 子任务停止时间
    private String operateType;// 操作类型：firmware_upgrade；software_upgrade
    private String deviceId;// 操作设备的设备ID
    private String status;// 子任务状态：wait,processing,fail,success,stop
    private String detailInfo;// 任务状态的详细描述，对于失败场景下为失败原因
    private Object extendInfo;// 任务扩展信息，视不同类型的操作不同
}
