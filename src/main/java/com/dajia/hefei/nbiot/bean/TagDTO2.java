package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
@Required
public class TagDTO2 {
    private String tagName;// 标签名称
    private String tagValue;// 标签值
}
