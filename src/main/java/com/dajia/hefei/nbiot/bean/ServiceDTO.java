package com.dajia.hefei.nbiot.bean;

import com.dajia.hefei.nbiot.util.DateTimeUtil;
import lombok.Data;

import java.util.List;

@Data
public class ServiceDTO {
    private String serviceId;// 设备的服务标识
    private String serviceType;// 设备的服务类型
    private Object data;// 属性值对
    private String eventTime;// 时间格式：yyyyMMdd'T'HHmmss'Z'
    private ServiceInfo serviceInfo;// 设备的服务信息

    public String getEventTime() {
        return DateTimeUtil.TZ2Punc(eventTime);
    }

    @Data
    public static class ServiceInfo {
        private List<String> muteCmds;// 设备控制命令列表
    }
}
