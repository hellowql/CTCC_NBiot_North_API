package com.dajia.hefei.nbiot.bean;

import lombok.Data;

import java.util.List;

@Data
public class ServiceCommandResponse {
    private String responseName;// 响应名称
    private List<ServiceCommandPara> paras;// 属性列表
}
