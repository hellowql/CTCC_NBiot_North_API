package com.dajia.hefei.nbiot.bean;

import lombok.Data;

@Data
public class SubscriptionDTO {
    private String subscriptionId;// 订阅ID，用于标识一个订阅
    private String notifyType;// 通知的类型
    private String callbackUrl;// 订阅的回调地址

}
